import { config } from 'dotenv';
import { Client, GatewayIntentBits, Events } from 'discord.js';
import { readyHandler } from './handler/readyHandler';
import { messageCreateHandler } from './handler/messageCreateHandler';
import { voiceStateUpdateHandler } from './handler/voiceStateUpdateHandler';
import { logger } from './lib/logger';

config();

logger.info(
  `${process.env.npm_package_name} ver ${process.env.npm_package_version}/${process.env.DCB_APP_ENV}`,
);

const options = {
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildVoiceStates,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMembers,
  ],
};
const client = new Client(options);

client.on(Events.ClientReady, readyHandler); // Discordサーバに接続完了時
client.on(Events.MessageCreate, messageCreateHandler); // テキストメッセージを受信時
// client.on(Events.GuildMemberAdd, () => { logger.trace(`GuildMemberAdd`)}) // メンバーがギルドに参加時
// client.on(Events.GuildMemberRemove, () => { logger.trace(`GuildMemberRemove`)}) // メンバーがギルドから退出時
// client.on(Events.InteractionCreate, () => { logger.info(`InteractionCreate`)}); // インタラクション(ボタンや選択メニュー)を実行時
client.on(Events.VoiceStateUpdate, voiceStateUpdateHandler); // ボイスチャンネルに誰かが参加/退出したとき

client.login(process.env.DCB_DISCORD_TOKEN);
