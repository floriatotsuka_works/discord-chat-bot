export enum COMMANDS {
  VC_IN = '/join',
  VC_OUT = '/bye',
  PREFIX = '/',
}

/**
 * ボイスチャンネルへの呼び出しコマンドかチェックする
 * @param message
 * @returns
 */
export const isVcInCommand = (message: string): boolean => {
  return message.indexOf(COMMANDS.VC_IN) === 0;
};

/**
 * ボイスチャンネルから退出コマンドかチェックする
 * @param message
 * @returns
 */
export const isVcOutCommand = (message: string): boolean => {
  return message.indexOf(COMMANDS.VC_OUT) === 0;
};

/**
 * 何らかのコマンドの可能性がある文字列かチェックする
 * @param message
 * @returns
 */
export const isSomeCommand = (message: string): boolean => {
  return message.indexOf(COMMANDS.PREFIX) === 0;
};
