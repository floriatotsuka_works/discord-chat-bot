/**
 * キュー(FIFO/待ち行列)のライブラリクラス。
 *
 * ※キューは機会があるときにResetしてください。インデックスがオーバーフローするかもしれません。
 */
export class Queue<T> {
  private queue: Map<number, T>;
  /** 先頭のインデックス番号(待ち行列がどこまで進んでいるか) */
  private front: number = 1;
  /** 最後尾のインデックス番号(待ち行列) */
  private back: number = 0;

  constructor() {
    this.front = 1;
    this.back = 0;
    this.queue = new Map();
  }

  /**
   * キューのサイズを取得する
   */
  get size(): number {
    return this.queue.size;
  }

  /**
   * 現在のキューのインデックス番号(待ち行列がどこまで進んでいるか)
   */
  get id(): number {
    return this.front;
  }

  /**
   * キューにオブジェクトを登録する。
   * @param value 登録するオブジェクト
   * @returns
   */
  enqueue(value: T): number {
    this.back++;
    this.queue.set(this.back, value);
    return this.back;
  }

  /**
   * キューから先頭のオブジェクトを取得する。
   * @returns
   */
  dequeue(): T {
    let poppedItem: T | null = null;
    while (!poppedItem) {
      if (this.front > this.back) {
        throw Error('End of Queue.');
      }
      // remove関数によってキューの途中がなくなっていた場合の対応
      const hasItem = this.queue.get(this.front);
      if (hasItem) {
        poppedItem = hasItem;
        break;
      } else {
        // 先頭が空だった場合は先頭インデックスを進める
        this.front++;
      }
    }
    this.queue.delete(this.front);
    this.front++;
    return poppedItem;
  }

  /**
   * 先頭のオブジェクト(空はスキップ)を取得する。
   *
   * ※デキューではないので、先頭のオブジェクトは削除されない。
   * @returns
   */
  current(): T | undefined {
    let peekedItem: T | undefined = undefined;
    while (!peekedItem) {
      if (this.front > this.back) {
        break;
      }
      // remove関数によってキューの先頭がなくなっていた場合の対応
      const hasItem = this.queue.get(this.front);
      if (hasItem) {
        peekedItem = hasItem;
        break;
      } else {
        // 先頭が空だった場合は先頭インデックスを進めてしまう
        this.front++;
      }
    }
    return peekedItem;
  }

  /**
   * キューの途中の要素を削除する
   * @param key 要素のインデックス番号
   */
  remove(key: number): void {
    const hasItem = this.queue.get(key);
    if (!hasItem) {
      throw TypeError('Item not in queue.');
    } else {
      this.queue.delete(key);
    }
  }
}
