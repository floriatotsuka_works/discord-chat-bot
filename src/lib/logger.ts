import log4js from 'log4js';

const logger = log4js.getLogger();

switch (process.env.DCB_APP_ENV) {
  case 'prd':
    logger.level = 'info';
    break;
  case 'stg':
    logger.level = 'debug';
    break;
  default:
    logger.level = 'trace';
    break;
}

export { logger };
