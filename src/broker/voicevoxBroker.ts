import axios from 'axios';
import { Readable } from 'stream';
import { logger } from '../lib/logger';

/**
 * テキストメッセージからモーラを生成する。
 * @param message
 * @returns
 */
export const retrieveMora = async (message: string) => {
  // モーラの生成
  const client = axios.create({
    baseURL: process.env.VOICEVOX_URL,
    timeout: 10000,
  });

  const query = '?text=' + encodeURI(message) + '&speaker=1';
  const response = await client.request({
    method: 'POST',
    url: '/audio_query' + query,
    headers: {
      accept: 'application/json',
    },
    data: '',
  });

  logger.trace(response);
  return response.data;
};

/**
 * モーラの入力から音声を生成する。
 * @param mora
 * @returns
 */
export const retrieveSpeechBinary = async (mora: any): Promise<Readable> => {
  // wavの生成
  const client = axios.create({
    baseURL: process.env.VOICEVOX_URL,
    timeout: 10000,
  });

  const query = '?speaker=1&enable_interrogative_upspeak=true';
  const newMora = configForSynthesis(mora);
  logger.trace(newMora);
  const response = await client.request({
    method: 'POST',
    url: '/synthesis' + query,
    responseType: 'arraybuffer',
    headers: {
      accept: 'audio/wav',
      'Content-Type': 'application/json',
    },
    data: newMora,
  });
  return response.data as Readable;
};

/**
 * モーラから音声生成するリクエストパラメータを変更する。
 * @param mora
 * @returns
 */
const configForSynthesis = (mora: JSON) => {
  const newMora = JSON.stringify(mora, replacer);
  return JSON.parse(newMora);
};

/**
 * VOICEVOXの音声生成向けのヘッダーパラメータの作成ルール。
 * @param key
 * @param value
 * @returns
 */
const replacer = (key: string, value: any) => {
  if (key === 'outputStereo') {
    return false;
    //   } else if (key === "outputSamplingRate") {
    //     return 16000;
  } else if (key === 'speedScale') {
    return 1.3;
  }
  return value;
};
