import { type VoiceState } from 'discord.js';
import { PrismaClient } from '@prisma/client';
import { isNull } from 'lodash';
import { logger } from '../lib/logger';
import { COMMANDS } from '../lib/command';
import { joinVoiceChannel } from '@discordjs/voice';

const prisma = new PrismaClient();

export const voiceStateUpdateHandler = async (
  oldState: VoiceState,
  newState: VoiceState,
) => {
  logger.info('VoiceStateUpdate');

  if (await isMyAction(newState)) {
    logger.trace('isMyAction');
    return;
  } else if (newState.member?.user.bot) {
    logger.trace('isOtherBotAction');
    return;
  }

  if (isJoinChannel(oldState, newState) && newState.channelId) {
    logger.trace('isJoinChannel');
    await registerVoiceChannelParticipant(newState);
  } else if (isMoveChannel(oldState, newState) && oldState.channelId) {
    logger.trace('isLeftChannel');
    await removeVoiceChannelParticipant(oldState);
    if (await isNobodyVoiceChannelParticipant(oldState)) {
      joinVoiceChannel({
        channelId: oldState.channelId,
        guildId: oldState.guild.id,
        adapterCreator: oldState.guild.voiceAdapterCreator,
      }).destroy();
      await removeClientOrder(oldState);
    }
    await registerVoiceChannelParticipant(newState);
  } else if (isLeftChannel(oldState, newState) && oldState.channelId) {
    logger.trace('isLeftChannel');
    await removeVoiceChannelParticipant(oldState);
    if (await isNobodyVoiceChannelParticipant(oldState)) {
      joinVoiceChannel({
        channelId: oldState.channelId,
        guildId: oldState.guild.id,
        adapterCreator: oldState.guild.voiceAdapterCreator,
      }).destroy();
      await removeClientOrder(oldState);
    }
  } else {
    logger.error('unknown operation');
    logger.error(oldState);
    logger.error(newState);
  }
  return;
};

/**
 * ボイスチャンネル上での参加判定(ボイスチャンネルに参加していない状態からの参加)。
 * @param oldState
 * @param newState
 * @returns
 */
const isJoinChannel = (oldState: VoiceState, newState: VoiceState): boolean => {
  return isNull(oldState.channelId) && !isNull(newState.channelId);
};

/**
 * ボイスチャンネル上での移動判定(どこかのボイスチャンネルから別のボイスチャンネルへの移動)。
 * @param oldState
 * @param newState
 * @returns
 */
const isMoveChannel = (oldState: VoiceState, newState: VoiceState): boolean => {
  return !isNull(oldState.channelId) && !isNull(newState.channelId);
};

/**
 * ボイスチャンネル上での退出判定(ボイスチャンネルに参加した状態からの退出)。
 * @param oldState
 * @param newState
 * @returns
 */
const isLeftChannel = (oldState: VoiceState, newState: VoiceState): boolean => {
  return !isNull(oldState.channelId) && isNull(newState.channelId);
};

/**
 * アプリが制御しているBOT自身の行動であるかの判定。
 * @param state
 * @returns
 */
const isMyAction = async (state: VoiceState): Promise<boolean> => {
  const myId = await getMyId();
  return myId === state.id;
};

/**
 * アプリが制御しているBOTのユーザIDを返す。
 * @returns BOTのユーザID
 */
const getMyId = async (): Promise<string> => {
  const app = await prisma.app.findMany();
  return app[0].app_id;
};

const registerVoiceChannelParticipant = async (
  state: VoiceState,
): Promise<void> => {
  if (isNull(state.guild.id)) {
    logger.error('guild ID dose not exist');
    return;
  } else if (isNull(state.channelId)) {
    logger.error('channel ID dose not exist');
    return;
  } else if (isNull(state.member?.user.id)) {
    logger.error('user ID dose not exist');
    return;
  }

  const participant = await prisma.voiceChannelParticipant.create({
    data: {
      guild_id: state.guild.id,
      guild_name: state.guild.name,
      channel_id: state.channelId,
      channel_name: state.channel?.name,
      user_id: state.member.user.id,
      user_name: state.member.user.username,
    },
  });
  logger.trace(participant);
};

const isNobodyVoiceChannelParticipant = async (
  state: VoiceState,
): Promise<boolean> => {
  if (isNull(state.guild.id)) {
    logger.error('guild ID dose not exist');
    return false;
  } else if (isNull(state.channelId)) {
    logger.error('channel ID dose not exist');
    return false;
  }

  const participant = await prisma.voiceChannelParticipant.findMany({
    where: {
      guild_id: state.guild.id,
      channel_id: state.channelId,
    },
  });
  logger.trace(participant);

  return participant.length === 0;
};

const removeVoiceChannelParticipant = async (
  state: VoiceState,
): Promise<void> => {
  if (isNull(state.guild.id)) {
    logger.error('guild ID dose not exist');
    return;
  } else if (isNull(state.channelId)) {
    logger.error('channel ID dose not exist');
    return;
  } else if (isNull(state.member?.user.id)) {
    logger.error('user ID dose not exist');
    return;
  }

  const participant = await prisma.voiceChannelParticipant.deleteMany({
    where: {
      guild_id: state.guild.id,
      channel_id: state.channelId,
      user_id: state.member.user.id,
    },
  });
  logger.trace(participant);
};

const removeClientOrder = async (state: VoiceState): Promise<void> => {
  if (isNull(state.guild.id)) {
    logger.error('guild ID dose not exist');
    return;
  } else if (isNull(state.channelId)) {
    logger.error('channel ID dose not exist');
    return;
  } else if (isNull(state.member?.user.id)) {
    logger.error('user ID dose not exist');
    return;
  }

  const participant = await prisma.clientOrder.deleteMany({
    where: {
      guild_id: state.guild.id,
      channel_id: state.channelId,
      user_id: state.member.user.id,
      order: COMMANDS.VC_IN,
    },
  });
  logger.trace(participant);
};
