import { type Message } from 'discord.js';
import { logger } from '../lib/logger';

/**
 * テキストチャンネル上で発信されたメッセージに対してdiscordレイヤーの操作をする。
 *
 * 前提: 入力されたメッセージはテキストチャンネル上で発信されたもの。
 * @param message
 */
export const textChannelHandler = async (message: Message) => {
  logger.debug(`${message.author.username}@TextChannel "${message.content}"`);
};
