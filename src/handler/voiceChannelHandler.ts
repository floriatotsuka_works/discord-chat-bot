import { type Message, ChannelType } from 'discord.js';
import { PrismaClient } from '@prisma/client';
import { retrieveMora, retrieveSpeechBinary } from '../broker/voicevoxBroker';
import { isVcInCommand, isVcOutCommand, COMMANDS } from '../lib/command';
import {
  AudioPlayerStatus,
  AudioResource,
  NoSubscriberBehavior,
  StreamType,
  createAudioPlayer,
  createAudioResource,
  joinVoiceChannel,
} from '@discordjs/voice';
import { logger } from '../lib/logger';
import { isNull } from 'lodash';
import { Readable } from 'stream';
import { Queue } from '../lib/queue';

const prisma = new PrismaClient();

/**
 * ボイスチャンネル上で発信されたメッセージに対してdiscordレイヤーの操作をする。
 *
 * 前提: 入力されたメッセージはボイスチャンネル上で発信されたもの。
 * @param message
 */
export const voiceChannelHandler = async (message: Message): Promise<void> => {
  logger.debug(`${message.author.username}@VoiceChannel "${message.content}"`);

  // message.guildチェックが冗長なのは、後続処理のnull例外処理の要求を黙らせるため。
  if (!hasRequiredParameter(message) || isNull(message.guild)) {
    return;
  }

  const vcMessage: VoiceChannelMessage = convertToVoiceChannelMessage(message);
  logger.trace(vcMessage);

  if (isVcInCommand(vcMessage.content)) {
    if (!(await isClientOnChannel(vcMessage))) {
      message.channel.send('ボイスチャンネルへの参加状態を確認できません。');
      return;
    }
    join(message, vcMessage);
    await registerClientOrderForSpokesman(vcMessage);
  } else if (isVcOutCommand(vcMessage.content)) {
    if (!(await isOrderOwnerForSpokesman(vcMessage))) {
      message.channel.send('退出操作をできる依頼主ではありません。');
      return;
    }
    leave(message, vcMessage);
    await removeClientOrderForSpokesman(vcMessage);
  } else if (
    (await isClientOnChannel(vcMessage)) &&
    (await inRoomForSpokesmanOrder(vcMessage))
  ) {
    await speechFromText(message);
  }

  return;
};

/**
 * ボイスチャンネル上のテキストメッセージで主に利用するパラメータ群。
 */
type VoiceChannelMessage = {
  guildId: string;
  guildname: string;
  channelId: string;
  channelname: string;
  userId: string;
  username: string;
  content: string;
};

/**
 * ボイスチャンネル上のテキストメッセージに必要なパラメータが揃っているか確認する。
 * @param message
 * @returns true: 問題なし / false: 問題あり
 */
const hasRequiredParameter = (message: Message): boolean => {
  if (isNull(message.guildId)) {
    logger.warn('guildId(message.guildId) dose not exist');
    return false;
  }
  if (isNull(message.guild)) {
    logger.warn('message.guild dose not exist');
    return false;
  }
  if (isNull(message.channelId)) {
    logger.warn('channelId(message.channelId) dose not exist');
    return false;
  }
  if (isNull(message.member?.user.id)) {
    logger.warn('userId(message.member?.user.id) dose not exist');
    return false;
  }
  if (isNull(message.guild.voiceAdapterCreator)) {
    logger.warn(
      'voiceAdapterCreator(message.guild.voiceAdapterCreator) dose not exist',
    );
    return false;
  }
  return true;
};

/**
 * 主に利用するパラメータを詰めなおす。
 * Messageのnullableなパラメータを空文字に変えるため、例外処理を軽減もできる。
 * @param message
 * @returns
 */
const convertToVoiceChannelMessage = (
  message: Message,
): VoiceChannelMessage => {
  if (isNull(message.guildId)) {
    logger.error('guild ID dose not exist');
  } else if (isNull(message.guild)) {
    logger.warn('message.guild dose not exist');
  } else if (isNull(message.channelId)) {
    logger.error('channel ID dose not exist');
  } else if (isNull(message.member?.user.id)) {
    logger.error('user ID dose not exist');
  }

  return {
    guildId: message.guildId || '',
    guildname: message.guild?.name || '',
    channelId: message.channelId,
    channelname:
      message.channel.type == ChannelType.GuildVoice
        ? message.channel.name
        : '',
    userId: message.member?.user.id || '',
    username: message.member?.user.username || '',
    content: message.content,
  };
};

/**
 * ボイスチャンネルへ参加する処理のラッパー
 * @param message
 * @param vcMessage
 * @returns
 */
const join = (message: Message, vcMessage: VoiceChannelMessage): void => {
  // message.guildの存在はチェック済みだが言語仕様上チェックする。
  if (isNull(message.guild)) {
    return;
  }

  joinVoiceChannel({
    channelId: vcMessage.channelId,
    guildId: vcMessage.guildId,
    adapterCreator: message.guild.voiceAdapterCreator,
  });
};

/**
 * ボイスチャンネルから退出する処理のラッパー
 * @param message
 * @param vcMessage
 * @returns
 */
const leave = (message: Message, vcMessage: VoiceChannelMessage): void => {
  // message.guildの存在はチェック済みだが言語仕様上チェックする。
  if (isNull(message.guild)) {
    return;
  }

  joinVoiceChannel({
    channelId: vcMessage.channelId,
    guildId: vcMessage.guildId,
    adapterCreator: message.guild.voiceAdapterCreator,
  }).destroy();
};

/**
 * ボイスチャンネル参加者管理DBからリクエストしたクライアントがボイスチャンネルに参加しているか確認する。
 * @param message
 * @returns
 */
const isClientOnChannel = async (
  message: VoiceChannelMessage,
): Promise<boolean> => {
  const participant = await prisma.voiceChannelParticipant.findMany({
    where: {
      guild_id: message.guildId,
      channel_id: message.channelId,
      user_id: message.userId,
    },
  });
  logger.trace(participant);

  return participant.length === 1;
};

/**
 * 代弁の依頼を依頼DBへ登録する。
 * @param message
 * @returns
 */
const registerClientOrderForSpokesman = async (
  message: VoiceChannelMessage,
): Promise<void> => {
  const participant = await prisma.clientOrder.create({
    data: {
      guild_id: message.guildId,
      guild_name: message.guildname,
      channel_id: message.channelId,
      channel_name: message.channelname,
      user_id: message.userId,
      user_name: message.username,
      order: COMMANDS.VC_IN,
    },
  });
  logger.trace(participant);

  return;
};

/**
 * 代弁の依頼主であるか確認する。
 * @param message
 * @returns
 */
const isOrderOwnerForSpokesman = async (
  message: VoiceChannelMessage,
): Promise<boolean> => {
  const participant = await prisma.clientOrder.findMany({
    where: {
      guild_id: message.guildId,
      channel_id: message.channelId,
      user_id: message.userId,
      order: COMMANDS.VC_IN,
    },
  });
  logger.trace(participant);

  return participant.length === 1;
};

/**
 * 代弁の依頼受託中であるか確認する。
 * @param message
 * @returns
 */
const inRoomForSpokesmanOrder = async (
  message: VoiceChannelMessage,
): Promise<boolean> => {
  const participant = await prisma.clientOrder.findMany({
    where: {
      guild_id: message.guildId,
      channel_id: message.channelId,
      order: COMMANDS.VC_IN,
    },
  });
  logger.trace(participant);

  return participant.length === 1;
};

/**
 * 代弁の依頼情報を依頼DBから消去する。
 * @param message
 * @returns
 */
const removeClientOrderForSpokesman = async (
  message: VoiceChannelMessage,
): Promise<void> => {
  const participant = await prisma.clientOrder.deleteMany({
    where: {
      guild_id: message.guildId,
      channel_id: message.channelId,
      user_id: message.userId,
      order: COMMANDS.VC_IN,
    },
  });
  logger.trace(participant);

  return;
};

/**
 * ボイスチャット読み上げ用のキュー
 */
const vcQueue: Queue<string> = new Queue();

/**
 * テキストから音声データを生成する。
 * @param message
 */
const speechFromText = async (message: Message) => {
  const index = vcQueue.enqueue(message.content);

  const mora = await retrieveMora(message.content);
  const voice = await retrieveSpeechBinary(mora);
  logger.trace(voice);
  // 先行する読み上げが処理中の場合は待つ。
  while (vcQueue.id !== index) {
    logger.trace(vcQueue.id + '/' + index + ', size:' + vcQueue.size);
    // 何らかの理由でキューが空になっていた場合は開放する。
    if (vcQueue.size === 0) {
      break;
    }
    await new Promise((resolve) => setTimeout(resolve, 100));
  }

  await speech(message, voice);
  vcQueue.dequeue();
  logger.trace(vcQueue.id + '/' + index + ', size:' + vcQueue.size);
};

/**
 * 音声再生するプレイヤー
 */
const audioPlayer = createAudioPlayer({
  behaviors: {
    noSubscriber: NoSubscriberBehavior.Pause,
  },
});

/**
 * 宛先情報と音声バイナリを受けてBOTに発話させる。
 * なお、先行して発話中の場合は発話が終わるまで、次の発話は待機する。
 * @param message 宛先情報源
 * @param binary 音声バイナリ
 * @returns
 */
const speech = async (message: Message, binary: Readable): Promise<void> => {
  // message.guildの存在はチェック済みだが言語仕様上チェックする。
  if (isNull(message.guild) || isNull(message.guildId)) {
    return;
  }

  const stream = new Readable();
  stream.push(binary);
  stream.push(null);
  const resource: AudioResource = createAudioResource(stream, {
    inputType: StreamType.Arbitrary,
  });

  // 再生中の音声がある場合は処理を待つ
  while (audioPlayer.state.status !== AudioPlayerStatus.Idle) {
    await new Promise((resolve) => setTimeout(resolve, 100));
  }

  audioPlayer.play(resource);
  joinVoiceChannel({
    channelId: message.channelId,
    guildId: message.guildId,
    adapterCreator: message.guild.voiceAdapterCreator,
  }).subscribe(audioPlayer);
};
