import { ChannelType, type Message } from 'discord.js';
import { logger } from '../lib/logger';
import { voiceChannelHandler } from './voiceChannelHandler';
import { textChannelHandler } from './textChannelHandler';

export const messageCreateHandler = async (message: Message) => {
  logger.info(
    `[Guild:${message.guild} Channel:${message.channel} ${message.author.username}(BOT:${message.author.bot})] ${message.content}`,
  );
  if (message.author.bot) return;

  switch (message.channel.type) {
    case ChannelType.GuildText:
      textChannelHandler(message);
      break;

    case ChannelType.GuildVoice:
      voiceChannelHandler(message);
      break;

    // case ChannelType.DM:
    //   break;

    default:
      logger.debug(message.channel.type.toString());
  }

  return;
};
