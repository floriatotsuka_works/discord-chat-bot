import { Client, ClientUser } from 'discord.js';
import { PrismaClient } from '@prisma/client';
import { logger } from '../lib/logger';
import axios from 'axios';

const prisma = new PrismaClient();

/**
 * BOTがdiscordサーバへ接続に成功した旨とBOTの情報をログ表示する。
 *
 * 前提: BOTクライアントが正常にサーバへ接続できたこと。
 * @param client
 */
export const readyHandler = async (client: Client) => {
  logger.debug('ready');
  if (client.user && client.user.id) {
    logger.info(`${client.user.tag} (ID:${client.user.id}) has logged in.`);

    initiateInfo(client.user)
      .then(async () => {
        await prisma.$disconnect();
      })
      .catch(async (e) => {
        logger.fatal('DCB database connection error.');
        logger.fatal(e);
        await prisma.$disconnect();
        process.exit(1);
      });
  } else {
    logger.fatal('Can not get an ID for BOT.');
    process.exit(1);
  }
};

/**
 * テーブルのレコード削除や周辺サーバとの接続チェックなどの初期化
 * @param user 初期データ情報源
 */
const initiateInfo = async (user: ClientUser): Promise<void> => {
  // VOICEVOXサービスへテスト接続
  const client = axios.create({
    baseURL: process.env.VOICEVOX_URL,
    timeout: 100,
  });

  client
    .request({
      method: 'GET',
      url: '/speakers',
      headers: {
        accept: 'application/json',
      },
      data: '',
    })
    .then((response) => {
      logger.trace(response);
      if (response.status !== 200) {
        logger.error(
          `The connection may not be established properly to VOICEVOX Service. (HTTP STATUS: ${response.status})`,
        );
      }
    })
    .catch((e) => {
      logger.fatal('VOICE BOX service connection error.');
      logger.fatal(e);
      process.exit(1);
    });

  // DBの初期化
  await prisma.app.deleteMany();
  const app = await prisma.app.upsert({
    where: { app_id: user.id },
    update: {
      app_id: user.id,
      app_name: process.env.npm_package_name || '',
      app_version: process.env.npm_package_version || '',
    },
    create: {
      app_id: user.id,
      app_name: process.env.npm_package_name || '',
      app_version: process.env.npm_package_version || '',
    },
  });
  await prisma.voiceChannelParticipant.deleteMany();
  await prisma.clientOrder.deleteMany();
  logger.trace(app);
};
