# Discord Chat Bot

Discordサーバ向けのBOTプログラムです。

## 主要機能

- [x] ボイスチャンネル中のテキストチャンネルのメッセージを、BOTが代わりに読み上げます。
    1. Botが起動中にボイスチャンネルに参加する。
    1. テキストメッセージとして/joinとメッセージを送信する。
    1. ボイスチャンネル参加者がテキストメッセージを送信するとBotが代わりに読み上げます。
    1. テキストメッセージとして/byeとメッセージを送信するか、ボイスチャンネルからユーザが居なくなるとBOTも退出します。

## はじめ方

このシステムはいくつかの外部サービスを前提としています。

### 必要環境

以下のプログラム言語や外部システムが必要です。

1. BOTプログラム実行環境に必要。

    - [x] TypeScript
    - [x] FFmpeg

2. 外部システムとして必要。

    - [x] [Discord Developer Platform](https://discord.com/developers/applications) Apps
    - [x] [VOICEVOX](https://voicevox.hiroshiba.jp/) Web API

### パッケージインストール

必要パッケージをインストールする。

```
npm install
```

### 環境設定

.envファイルを作成し、パラメータを下記の例のように設定する。

```
DCB_APP_ENV = "dev"                     # stgかprdで出力ログレベルを指定(他の設定値なら全てログ出力)
DCB_DISCORD_TOKEN = "***"               # DiscordアプリケーションのToken
DCB_DC_PATH = "file:./dev.db"           # DBデータを配置するファイルパス
VOICEVOX_URL = "http://localhost:50021" # VOICEVOXサービスのURL
```

### 初期データベース構築

データベースを初期設定します。

```
npm run setup
```

### 実行する

```
npm run dev
```

## ライセンス

Discord Chat Botは[MITライセンス](LICENSE)に準じます。
